package newsServer.newsServer.DTO;

public class NewNewsDTO {

    private String title;

    private String news;

    private String authorEmail;

    private Integer newsTagId;

    public NewNewsDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public Integer getNewsTagId() {
        return newsTagId;
    }

    public void setNewsTagId(Integer newsTagId) {
        this.newsTagId = newsTagId;
    }
}
