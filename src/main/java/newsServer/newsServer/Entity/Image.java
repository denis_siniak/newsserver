package newsServer.newsServer.Entity;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Image {

    @Id
    @Column(name = "image")
    private String image;

    @ManyToOne
    @JoinColumn(name = "news_id")
    private News news;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }
}
