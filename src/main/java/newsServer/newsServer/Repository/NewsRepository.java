package newsServer.newsServer.Repository;

import newsServer.newsServer.Entity.Author;
import newsServer.newsServer.Entity.News;
import newsServer.newsServer.Entity.NewsTag;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NewsRepository extends CrudRepository<News, Integer> {

    List<News> findAllByIdIsAfterOrderByIdDesc(Integer id);
    List<News> findAllByAuthorOrderByIdDesc(Author author);
    List<News> findAllByNewsTagOrderByIdDesc(NewsTag newsTag);
    List<News> findAllByTitleIsLikeOrderByIdDesc(String search);
    News getFirstByIdIsAfterOrderByIdDesc(Integer id);

}
