package newsServer.newsServer.Repository;

import newsServer.newsServer.Entity.NewsTag;
import org.springframework.data.repository.CrudRepository;

public interface NewsTagRepository extends CrudRepository<NewsTag, Integer> {

    NewsTag findByName(String name);
}
