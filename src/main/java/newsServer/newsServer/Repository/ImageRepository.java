package newsServer.newsServer.Repository;

import newsServer.newsServer.Entity.Image;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ImageRepository extends CrudRepository<Image, String> {

//    Image findByNewsId(Integer id);
//    List<Image> findAllByNewsId(Integer id);
}
