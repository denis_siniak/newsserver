package newsServer.newsServer.Repository;

import newsServer.newsServer.Entity.Bookmarks;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookmarksRepository extends CrudRepository<Bookmarks, Integer> {

    List<Bookmarks> findAllByUserEmailOrderByNewsIdDesc(String email);

    Boolean existsByUserEmailAndNewsId(String email, Integer id);
    Bookmarks findByUserEmailAndNewsId(String email, Integer id);
}
