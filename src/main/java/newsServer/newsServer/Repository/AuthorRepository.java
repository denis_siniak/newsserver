package newsServer.newsServer.Repository;

import newsServer.newsServer.Entity.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Integer> {

    Author findByEmail(String email);
    Boolean existsByEmail(String email);

}
