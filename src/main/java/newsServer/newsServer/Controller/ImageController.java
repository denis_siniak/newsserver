package newsServer.newsServer.Controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.io.*;

@RestController
@RequestMapping("image/*")
public class ImageController {

    @ResponseBody
    @RequestMapping(value = "get/{imageName}")
    public ResponseEntity<byte[]> getImage(@PathVariable("imageName") String imageName) throws IOException{

        String path = "images/";
        ByteArrayOutputStream out = null;
        InputStream input = null;
        try{
            out = new ByteArrayOutputStream();
            input = new BufferedInputStream(new FileInputStream(path + File.separator + imageName));
            int data = 0;
            while ((data = input.read()) != -1){
                out.write(data);
            }
        }
        finally{
            if (null != input){
                input.close();
            }
            if (null != out){
                out.close();
            }
        }
        byte[] bytes = out.toByteArray();

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);

        return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.CREATED);
    }
}

//try {
//            String path = "images/";
//            File file = new File(path + imageName);
//            BufferedImage bufferedImage = ImageIO.read(file);
//            WritableRaster raster = bufferedImage.getRaster();
//            DataBufferByte data = (DataBufferByte) raster.getDataBuffer();
//
//
//
//            return new ResponseEntity<>(data.getData(), HttpStatus.OK);
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }