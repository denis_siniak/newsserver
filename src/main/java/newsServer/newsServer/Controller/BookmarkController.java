package newsServer.newsServer.Controller;

import newsServer.newsServer.DTO.BookmarkDTO;
import newsServer.newsServer.Service.BookmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("bookmarks/*")
public class BookmarkController {

    private final BookmarkService bookmarkService;

    @Autowired
    public BookmarkController(BookmarkService bookmarkService) {
        this.bookmarkService = bookmarkService;
    }

    @RequestMapping(value = "isInBookmark", method = RequestMethod.POST)
    public Boolean isInBookmark(@RequestBody BookmarkDTO bookmarkDTO){
        return bookmarkService.isInBookmark(bookmarkDTO);
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public Boolean add(@RequestBody BookmarkDTO bookmarkDTO){
        return bookmarkService.add(bookmarkDTO);
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public Boolean remove(@RequestBody BookmarkDTO bookmarkDTO){
        return bookmarkService.remove(bookmarkDTO);
    }
}
