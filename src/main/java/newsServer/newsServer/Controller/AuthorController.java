package newsServer.newsServer.Controller;

import newsServer.newsServer.DTO.NewNewsDTO;
import newsServer.newsServer.Service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("author/*")
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @RequestMapping("isAuthor/{email}")
    public Boolean isAuthor(@PathVariable("email")String email){
        return authorService.isAuthor(email);
    }

    @RequestMapping("add")
    public Boolean add(@RequestPart NewNewsDTO newsDTO, @RequestPart List<MultipartFile> images){
        return authorService.add(newsDTO, images);
    }


}
