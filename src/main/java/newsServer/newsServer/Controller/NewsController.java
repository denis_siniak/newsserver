package newsServer.newsServer.Controller;

import newsServer.newsServer.DTO.NewsDTO;
import newsServer.newsServer.Service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("news/*")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping("getByTag/{tag}")
    public List<NewsDTO> getNewsListByTag(@PathVariable("tag")String tag){
        return newsService.getNewsListByTag(tag);
    }

    @RequestMapping("getByAuthor/{author}")
    public List<NewsDTO> getNewsListByAuthor(@PathVariable("author")String authorEmail){
        return newsService.getNewsListByAuthor(authorEmail);
    }

    @RequestMapping("getBySearch/{search}")
    public List<NewsDTO> getNewsListBySearch(@PathVariable("search")String search){
        return newsService.getNewsListBySearch(search);
    }

    @RequestMapping("getBookmarks/{userEmail}")
    public List<NewsDTO> getBookmarks(@PathVariable("userEmail")String email){
        return newsService.getBookmarks(email);
    }

    @RequestMapping("getById/{id}")
    public NewsDTO getNewsById(@PathVariable("id")Integer id){
        return newsService.getNewsById(id);
    }
}
