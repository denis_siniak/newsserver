package newsServer.newsServer.Service;

import newsServer.newsServer.DTO.NewNewsDTO;
import newsServer.newsServer.Entity.Author;
import newsServer.newsServer.Entity.Image;
import newsServer.newsServer.Entity.News;
import newsServer.newsServer.Entity.NewsTag;
import newsServer.newsServer.Repository.AuthorRepository;
import newsServer.newsServer.Repository.ImageRepository;
import newsServer.newsServer.Repository.NewsRepository;
import newsServer.newsServer.Repository.NewsTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final NewsRepository newsRepository;
    private final NewsTagRepository newsTagRepository;
    private final ImageRepository imageRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository, NewsRepository newsRepository, NewsTagRepository newsTagRepository, ImageRepository imageRepository) {
        this.authorRepository = authorRepository;
        this.newsRepository = newsRepository;
        this.newsTagRepository = newsTagRepository;
        this.imageRepository = imageRepository;
    }

    public Boolean isAuthor(String email){
        return authorRepository.existsByEmail(email);
    }

    public Boolean add(NewNewsDTO newNewsDTO, List<MultipartFile> images){
        News news = new News();
        Author author = authorRepository.findByEmail(newNewsDTO.getAuthorEmail());
        NewsTag newsTag = newsTagRepository.findById(newNewsDTO.getNewsTagId()).get();
        List<String> imagePaths = new ArrayList<>();
        String path = "images/";

        if (!images.isEmpty()){
            try {
                for (MultipartFile multipartFile: images){
                    byte[] bytes = multipartFile.getBytes();
                    String fileName = multipartFile.getOriginalFilename();
                    int i = 0;
                    while (imageRepository.existsById(fileName)){
                        fileName = + i + multipartFile.getOriginalFilename();
                        i++;
                    }

                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(path + fileName)));
                    stream.write(bytes);
                    imagePaths.add(fileName);
                    stream.close();
                }
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

        news.setTitle(newNewsDTO.getTitle());
        news.setNews(newNewsDTO.getNews());
        news.setNewsTag(newsTag);
        news.setAuthor(author);
        news.setDate(new Date());

        newsRepository.save(news);

        for (String image: imagePaths){
            Image item = new Image();
            item.setImage(image);
            item.setNews(newsRepository.getFirstByIdIsAfterOrderByIdDesc(0));
            imageRepository.save(item);
        }

        return true;
    }
}
