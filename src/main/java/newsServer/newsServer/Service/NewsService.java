package newsServer.newsServer.Service;

import newsServer.newsServer.DTO.AuthorDTO;
import newsServer.newsServer.DTO.NewsDTO;
import newsServer.newsServer.Entity.Bookmarks;
import newsServer.newsServer.Entity.Image;
import newsServer.newsServer.Entity.News;
import newsServer.newsServer.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class NewsService {

    private final NewsRepository newsRepository;
    private final NewsTagRepository newsTagRepository;
    private final AuthorRepository authorRepository;
    private final ImageRepository imageRepository;
    private final BookmarksRepository bookmarksRepository;

    @Autowired
    public NewsService(NewsRepository newsRepository, NewsTagRepository newsTagRepository, AuthorRepository authorRepository, ImageRepository imageRepository, BookmarksRepository bookmarksRepository) {
        this.newsRepository = newsRepository;
        this.newsTagRepository = newsTagRepository;
        this.authorRepository = authorRepository;
        this.imageRepository = imageRepository;
        this.bookmarksRepository = bookmarksRepository;
    }

    public List<NewsDTO> getNewsListByTag(String tag) {
        if (tag.equals("Новости")) {
            return createNewsDTOs(newsRepository.findAllByIdIsAfterOrderByIdDesc(0));
        }
        return createNewsDTOs(newsRepository.findAllByNewsTagOrderByIdDesc(newsTagRepository.findByName(tag)));
    }

    public List<NewsDTO> getNewsListByAuthor(String authorEmail) {
        return createNewsDTOs(newsRepository.findAllByAuthorOrderByIdDesc(authorRepository.findByEmail(authorEmail)));
    }


    public List<NewsDTO> getNewsListBySearch(String search) {
        return createNewsDTOs(newsRepository.findAllByTitleIsLikeOrderByIdDesc("%" + search + "%"));
    }

    public List<NewsDTO> getBookmarks(String email) {
        List<News> news = new ArrayList<>();
        for (Bookmarks bookmarks : bookmarksRepository.findAllByUserEmailOrderByNewsIdDesc(email)) {
            news.add(newsRepository.findById(bookmarks.getNewsId()).get());
        }

        return createNewsDTOs(news);
    }

    public NewsDTO getNewsById(Integer id) {
        List<News> news = new ArrayList<>();
        news.add(newsRepository.findById(id).get());
        return createNewsDTOs(news).get(0);
    }

    private List<NewsDTO> createNewsDTOs(List<News> news) {
        List<NewsDTO> newsDTOs = new ArrayList<>();
        for (News item : news) {
            NewsDTO newsDTO = new NewsDTO();
            AuthorDTO authorDTO = new AuthorDTO();
            List<String> images = new ArrayList<>();

            for (Image image : item.getImages())
                images.add(image.getImage());

            authorDTO.setEmail(item.getAuthor().getEmail());
            authorDTO.setName(item.getAuthor().getName());
            authorDTO.setPosition(item.getAuthor().getPosition());

            newsDTO.setId(item.getId());
            newsDTO.setTitle(item.getTitle());
            newsDTO.setNews(item.getNews());
            newsDTO.setImages(images);
            newsDTO.setAuthor(authorDTO);
            newsDTO.setDate(getTime(item.getDate()));

            newsDTOs.add(newsDTO);
        }

        return newsDTOs;
    }


    public static String getTime(Date dateEvent) {
        Calendar now = Calendar.getInstance();
        Calendar event = Calendar.getInstance();
        now.setTime(new Date());
        event.setTime(dateEvent);

        long mil = now.getTimeInMillis() - event.getTimeInMillis();

        if (mil < 60000) {
            int seconds = (int) (mil / 1000);

            if (seconds > 11 && seconds < 19)
                return seconds + " секунд назад";

            switch (seconds % 10) {
                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:{
                    return seconds + " секунд назад";
                }
                case 1: {
                    return seconds + " секунду назад";
                }
                case 2:
                case 3:
                case 4:{
                    return seconds + " секунды назад";
                }
            }
        }

        if (mil < 3600000) {
            int minutes = (int) (mil / 60000);

            if (minutes > 11 && minutes < 19)
                return minutes + " минут назад";

            switch (minutes % 10) {
                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:{
                    return minutes + " минут назад";
                }
                case 1: {
                    return minutes + " минуту назад";
                }
                case 2:
                case 3:
                case 4:{
                    return minutes + " минуты назад";
                }
            }
        }

        if (mil < 86400000) {
            int hours = (int) (mil / 3600000);

            if (hours > 11 && hours < 19)
                return hours + " часов назад";

            switch (hours % 10) {
                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:{
                    return hours + " часов назад";
                }
                case 1: {
                    return hours + " час назад";
                }
                case 2:
                case 3:
                case 4:{
                    return hours + " часа назад";
                }
            }
        }

        if (mil < 172800000)
            return "Вчера";

        return dateEvent.toString();
    }

}
