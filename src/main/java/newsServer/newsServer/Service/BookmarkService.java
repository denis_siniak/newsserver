package newsServer.newsServer.Service;

import newsServer.newsServer.DTO.BookmarkDTO;
import newsServer.newsServer.Entity.Bookmarks;
import newsServer.newsServer.Repository.BookmarksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookmarkService {

    private final BookmarksRepository bookmarksRepository;

    @Autowired
    public BookmarkService(BookmarksRepository bookmarksRepository) {
        this.bookmarksRepository = bookmarksRepository;
    }

    public Boolean isInBookmark(BookmarkDTO bookmarkDTO){
        return bookmarksRepository.existsByUserEmailAndNewsId(bookmarkDTO.getUserEmail(), bookmarkDTO.getNewsId());
    }

    public Boolean add(BookmarkDTO bookmarkDTO){
        Bookmarks bookmarks = new Bookmarks();
        bookmarks.setUserEmail(bookmarkDTO.getUserEmail());
        bookmarks.setNewsId(bookmarkDTO.getNewsId());
        bookmarksRepository.save(bookmarks);

        return true;
    }

    public Boolean remove(BookmarkDTO bookmarkDTO){
        Bookmarks bookmarks = bookmarksRepository.findByUserEmailAndNewsId(bookmarkDTO.getUserEmail(), bookmarkDTO.getNewsId());
        bookmarksRepository.delete(bookmarks);

        return true;
    }
}
